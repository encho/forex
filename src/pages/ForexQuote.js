import React from "react";
import { Link } from "react-router-dom";
import api from "../api";
import Quote from "../components/Quote";

class ForexQuote extends React.Component {
  state = {
    data: null
  };

  componentDidMount() {
    const base = this.props.match.params.base;
    const terms = this.props.match.params.terms;
    api.getQuote(base, terms).then(data => this.setState({ data }));
  }

  render() {
    const base = this.props.match.params.base;
    const terms = this.props.match.params.terms;
    return (
      <div>
        <Link to="/">Back to Forex list</Link>
        {this.state.data ? (
          <Quote base={base} terms={terms} quote={this.state.data.quote} />
        ) : (
          <p>loading..</p>
        )}
      </div>
    );
  }
}

export default ForexQuote;
