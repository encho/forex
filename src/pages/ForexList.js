import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const ForexList = () => {
  return (
    <div>
      <h1>ForexList</h1>
      <ul>
        <li>
          <Link to="/EUR/JPY">EUR-JPY</Link>
        </li>
        <li>
          <Link to="/EUR/USD">EUR-USD</Link>
        </li>
      </ul>
    </div>
  );
};

export default ForexList;
