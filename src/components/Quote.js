import React from "react";

const Quote = ({ base, terms, quote }) => (
  <div>
    <h3>
      {base}-{terms} Quote
    </h3>
    <p>{quote}</p>
  </div>
);

export default Quote;
