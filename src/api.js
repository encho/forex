// mocked getQuote implementation
const getQuote = (base, terms) => {
  const promise = new Promise(resolve => {
    setTimeout(() => resolve({ base, terms, quote: Math.random() }), 1500);
  });

  return promise;
};

export default {
  getQuote
};
