import React from "react";
import ForexList from "./pages/ForexList";
import ForexQuote from "./pages/ForexQuote";
import { BrowserRouter as Router, Route } from "react-router-dom";

const App = () => {
  return (
    <Router>
      <Route exact path="/" component={ForexList} />
      <Route exact path="/:base/:terms" component={ForexQuote} />
    </Router>
  );
};

export default App;
