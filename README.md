This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Run locally

You need node, npm, yarn installed locally.

In the project directory, you can run:

### `yarn`

Installs all the necessary dependencies into the `node_modules` folder.

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

## Deployed app

Find a deployed version of the app running [here](https://forex-tcdcitkmde.now.sh).
